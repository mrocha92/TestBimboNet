package com.example.testbimbonet.presentation.home;

import android.util.Pair;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.testbimbonet.data.generic.BaseModel;
import com.example.testbimbonet.data.network.repository.BeersRepository;
import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;
import java.util.List;
import javax.inject.Inject;
import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class HomeViewModel extends ViewModel {


    @Inject
    BeersRepository beersRepository;


    @Inject
    public HomeViewModel(BeersRepository beersRepository) {
        this.beersRepository = beersRepository;
    }
    public MutableLiveData<Pair<BaseModel, List<GetBeersResponseModelItem>>> getBeersResult = new MutableLiveData<>(
            new Pair<>(new BaseModel(), null));

    public void getBeersList(String page) {
        beersRepository.getBeersList(page, beerResponseModels -> getBeersResult.setValue(beerResponseModels));
    }
}
