package com.example.testbimbonet.presentation.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.testbimbonet.R;
import com.example.testbimbonet.databinding.FragmentDetailBeerBinding;
import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;
import com.example.testbimbonet.presentation.mainView.MainActivityViewModel;

import coil.Coil;
import coil.ImageLoader;
import coil.request.ImageRequest;
import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DetailBeerFragment extends Fragment {

    public DetailBeerFragment() {
        // Required empty public constructor
    }

    FragmentDetailBeerBinding binding;
    MainActivityViewModel mainViewModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailBeerBinding.inflate(inflater, container, false);
        initElements();
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {
        GetBeersResponseModelItem detailBeer = mainViewModel.beerItem.getValue();
        String abv = detailBeer.getAbv() + "%";
        String ibu = detailBeer.getIbu() + "%";
        String ebc = detailBeer.getEbc() + "%";
        String ph = detailBeer.getPh() + "%";
        String contributed = detailBeer.getContributed_by();
        binding.txtNameBeer.setText(detailBeer.getName());
        binding.txtDescription.setText(detailBeer.getDescription());
        binding.txtAbvValue.setText(abv);
        binding.txtIbuValue.setText(ibu);
        binding.txtEbcValue.setText(ebc);
        binding.txtPhValue.setText(ph);
        binding.txtBrewersTip.setText(detailBeer.getBrewers_tips());
        binding.txtContributed.setText(getString(R.string.contributed, contributed));

        String liters = detailBeer.getValue() +" "+ detailBeer.getUnit();
        binding.txtLiters.setText(liters);

        ImageLoader imageLoader = Coil.imageLoader(binding.imgBeer.getContext());
        ImageRequest request = new ImageRequest.Builder(binding.imgBeer.getContext())
                .data(detailBeer.image_url)
                .crossfade(true)
                .target(binding.imgBeer)
                .build();
        imageLoader.enqueue(request);




    }


    private void initElements() {
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);
    }
}