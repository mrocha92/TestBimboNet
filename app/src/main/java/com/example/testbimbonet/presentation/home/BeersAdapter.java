package com.example.testbimbonet.presentation.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testbimbonet.databinding.ItemBearBinding;
import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;

import coil.Coil;
import coil.ImageLoader;
import coil.request.ImageRequest;

public class BeersAdapter extends ListAdapter<GetBeersResponseModelItem, BeersAdapter.ViewHolder> {

    private final BeersAdapter.OnItemClickListener listener;

    public BeersAdapter(OnItemClickListener listener) {
        super(Comparator);
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBearBinding binding = ItemBearBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GetBeersResponseModelItem item = getItem(position);
        if (item != null) {
            holder.bind(item, listener);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemBearBinding binding;

        ViewHolder(ItemBearBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(GetBeersResponseModelItem itemData, BeersAdapter.OnItemClickListener listener) {
            binding.getRoot().setOnClickListener(v -> listener.onItemClick(itemData));
            binding.nameBeer.setText(itemData.getName());
            binding.tagline.setText(itemData.getTagline());

            ImageLoader imageLoader = Coil.imageLoader(binding.imgProfile.getContext());
            ImageRequest request = new ImageRequest.Builder(binding.imgProfile.getContext())
                    .data(itemData.image_url)
                    .crossfade(true)
                    .target(binding.imgProfile)
                    .build();
            imageLoader.enqueue(request);
        }
    }

    public static final DiffUtil.ItemCallback<GetBeersResponseModelItem> Comparator =
            new DiffUtil.ItemCallback<GetBeersResponseModelItem>() {
                @Override
                public boolean areItemsTheSame(@NonNull GetBeersResponseModelItem oldItem, @NonNull GetBeersResponseModelItem newItem) {
                    return oldItem.getId().equals(newItem.getId());
                }

                @Override
                public boolean areContentsTheSame(@NonNull GetBeersResponseModelItem oldItem, @NonNull GetBeersResponseModelItem newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public interface OnItemClickListener {
        void onItemClick(GetBeersResponseModelItem item);
    }
}

