package com.example.testbimbonet.presentation.login;

import static com.example.testbimbonet.domain.local.PreferencesFieldName.prefFile;
import static com.example.testbimbonet.domain.local.PreferencesFieldName.usr;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.testbimbonet.databinding.FragmentLoginBinding;
import com.example.testbimbonet.domain.local.PreferencesHelper;
import com.example.testbimbonet.presentation.utils.Utils;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class LoginFragment extends Fragment {

    public LoginFragment() {
    }

    private FragmentLoginBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {

        binding.etUser.setText(getUsr());
        binding.btnLogin.setOnClickListener(v -> {

            String txtUser = binding.etUser.getText().toString().trim();
            String txtPassword = binding.etPassword.getText().toString().trim();

            Utils utils = new Utils();

            if (txtUser.equals("mar") && txtPassword.equals("1234")) {
                setUsr(txtUser);

                NavDirections action = LoginFragmentDirections.actionLoginF0ragmentToHomeFragment();
                Navigation.findNavController(v).navigate(action);
            } else if (!txtUser.isEmpty() && !txtPassword.isEmpty()) {

                Toast.makeText(requireActivity(), "Usuario o Constraseña no coinciden", Toast.LENGTH_SHORT).show();

                utils.validField(txtUser, requireActivity(), binding.inputLayoutEdTxtUser);
                utils.validField(txtPassword, requireActivity(), binding.inputLayoutEdTxtPassword);
            } else {
                utils.validField(txtUser, requireActivity(), binding.inputLayoutEdTxtUser);
                utils.validField(txtPassword, requireActivity(), binding.inputLayoutEdTxtPassword);
            }
        });
    }


    public void setUsr(String txtUser) {
        PreferencesHelper preferencesHelper = new PreferencesHelper(requireActivity());
        preferencesHelper.setSharedPreferenceString(prefFile, usr, txtUser);

    }

    public String getUsr() {
        PreferencesHelper preferencesHelper = new PreferencesHelper(requireActivity());
        return preferencesHelper.getSharedPreferenceString(prefFile, usr, "");
    }
}