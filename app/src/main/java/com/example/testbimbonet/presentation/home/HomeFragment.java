package com.example.testbimbonet.presentation.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.testbimbonet.databinding.FragmentHomeBinding;
import com.example.testbimbonet.presentation.mainView.MainActivityViewModel;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class HomeFragment extends Fragment {

    public HomeFragment() {}

    private FragmentHomeBinding binding;
    HomeViewModel viewModel;
    MainActivityViewModel mainViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        initElements();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BeersAdapter adapter = new BeersAdapter(beerItem -> {
            mainViewModel.beerItem.setValue(beerItem);
            changeFragment(view);
        });

        binding.rvRepos.setAdapter(adapter);

        viewModel.getBeersList("1");

        viewModel.getBeersResult.observe(getViewLifecycleOwner(),
                beerList -> {
                    if (beerList.first.status == 200) {
                        // show recycler
                        adapter.submitList(beerList.second);
                    } else {
                    }
                });
    }

    private void changeFragment(View view) {
        NavDirections action = HomeFragmentDirections.actionHomeFragmentToDetailBeerFragment();
        Navigation.findNavController(view).navigate(action);
    }

    private void initElements() {
        viewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);
    }
}
