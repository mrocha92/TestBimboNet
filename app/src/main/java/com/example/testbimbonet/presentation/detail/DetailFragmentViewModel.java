package com.example.testbimbonet.presentation.detail;


import androidx.lifecycle.ViewModel;
import com.example.testbimbonet.data.network.repository.BeersRepository;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class DetailFragmentViewModel extends ViewModel {


    @Inject
    BeersRepository beersRepository;

    @Inject
    public DetailFragmentViewModel(BeersRepository beersRepository) {
        this.beersRepository = beersRepository;
    }
}