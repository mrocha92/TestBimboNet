package com.example.testbimbonet.presentation.utils;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.example.testbimbonet.R;
import com.google.android.material.textfield.TextInputLayout;

public class Utils {

    public void validField(String string, Activity context, TextInputLayout edTxt) {
        if (string.isEmpty()) {
            edTxt.setHelperText(context.getString(R.string.empty_field));
        } else {
            edTxt.setHelperText("");
        }
    }

    public void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
