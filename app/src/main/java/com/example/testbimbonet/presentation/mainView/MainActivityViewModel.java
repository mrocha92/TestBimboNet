package com.example.testbimbonet.presentation.mainView;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class MainActivityViewModel extends ViewModel {

    @Inject
    public MainActivityViewModel(){}
    public MutableLiveData<GetBeersResponseModelItem> beerItem = new MutableLiveData<>();
}
