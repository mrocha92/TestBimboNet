package com.example.testbimbonet.data.di;


import com.example.testbimbonet.BuildConfig;
import com.example.testbimbonet.data.network.repository.BeersRepository;
import com.example.testbimbonet.data.network.repository.BeersRepositoryImpl;
import com.example.testbimbonet.data.service.GenericServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@InstallIn(SingletonComponent.class)
@Module
public class NetworkModule {


    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor httpLoginInterceptor = new HttpLoggingInterceptor();
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoginInterceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public Gson providesGson() {
        return new GsonBuilder().create();
    }

    @Provides
    public GsonBuilder providesGsonBuilder() {
        return new GsonBuilder();
    }


    @Singleton
    @Provides
    public Retrofit privideRetrofitCLient(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
               .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }


    @Singleton
    @Provides
    public GenericServices providesGenericServicesImplementation(Retrofit retrofit) {
        return retrofit.create(GenericServices.class);
    }

    @Singleton
    @Provides
    public BeersRepository providesReadmeRepository(BeersRepositoryImpl impl) {
        return impl;
    }

}