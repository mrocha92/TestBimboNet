package com.example.testbimbonet.data.network.repository;


import android.content.Context;
import android.util.Pair;

import androidx.lifecycle.Observer;

import com.example.testbimbonet.data.generic.BaseModel;
import com.example.testbimbonet.data.room.AppDataBase;
import com.example.testbimbonet.data.room.BeerEntity;
import com.example.testbimbonet.data.service.GenericServices;
import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeersRepositoryImpl implements BeersRepository {

    private final GenericServices genericServices;

    private final Context context;

    @Inject
    public BeersRepositoryImpl(GenericServices genericServices, @ApplicationContext Context context) {
        this.genericServices = genericServices;
        this.context = context;
    }

    @Override
    public void getBeersList(String page, Observer<Pair<BaseModel, List<GetBeersResponseModelItem>>> executeBeerData) {
        List<BeerEntity> list = AppDataBase.getDatabase(context).beerDao().getBeerListRoom();

        if (list.isEmpty()) {
            getNetworkBeerList(page, executeBeerData);
        } else {
            getLocalBeerList(list, executeBeerData);
        }
    }

    private void getNetworkBeerList(String page, Observer<Pair<BaseModel, List<GetBeersResponseModelItem>>> executeBeerData) {
        genericServices.getBearsList(page).enqueue(new Callback<List<GetBeersResponseModelItem>>() {
            @Override
            public void onResponse(Call<List<GetBeersResponseModelItem>> call, Response<List<GetBeersResponseModelItem>> response) {
                List<GetBeersResponseModelItem> beerResponseList;
                try {
                    beerResponseList = response.body();
                } catch (Exception e) {
                    beerResponseList = new ArrayList<>();
                }
                setInDB(context, beerResponseList);
                executeBeerData.onChanged(new Pair<>(new BaseModel(response.code(), response.message()), beerResponseList));
            }

            @Override
            public void onFailure(Call<List<GetBeersResponseModelItem>> call, Throwable t) {
                executeBeerData.onChanged(new Pair<>(new BaseModel(t.hashCode(), t.getMessage()), null));
            }
        });
    }

    private void setInDB(Context context, List<GetBeersResponseModelItem> list) {
        List<BeerEntity> entityList = new ArrayList<>();
        list.forEach(item -> {
            BeerEntity beerItemModel = new BeerEntity();
            beerItemModel.setId(item.getId());
            beerItemModel.setName(item.getName());
            beerItemModel.setDescription(item.getDescription());
            beerItemModel.setTagline(item.getTagline());
            beerItemModel.setImage_url(item.getImage_url());
            beerItemModel.setYeast(item.getIngredients().getYeast());
            beerItemModel.setValue(item.getVolume().getValue());
            beerItemModel.setUnit(item.getVolume().getUnit());
            beerItemModel.setAbv(item.getAbv());
            beerItemModel.setIbu(item.getIbu());
            beerItemModel.setEbc(item.getEbc());
            beerItemModel.setPh(item.getPh());
            beerItemModel.setTarget_og(item.getTarget_og());
            beerItemModel.setTarget_fg(item.getTarget_fg());
            beerItemModel.setFirst_brewed(item.getFirst_brewed());
            beerItemModel.setBrewers_tips(item.getBrewers_tips());
            beerItemModel.setContributed_by(item.getContributed_by());
            String foodPairing = item.getFood_pairing().toString();
            beerItemModel.setFood_pairing(foodPairing);

            entityList.add(beerItemModel);
        });

        AppDataBase.getDatabase(context).beerDao().inserAll(entityList);
    }

    private void getLocalBeerList(List<BeerEntity> list, Observer<Pair<BaseModel, List<GetBeersResponseModelItem>>> executeBeerData) {
        List<GetBeersResponseModelItem> localList = new ArrayList<>();
        list.forEach(item -> {
            GetBeersResponseModelItem beerItemModel = new GetBeersResponseModelItem();
            beerItemModel.setId(item.getId());
            beerItemModel.setName(item.getName());
            beerItemModel.setDescription(item.getDescription());
            beerItemModel.setTagline(item.getTagline());
            beerItemModel.setImage_url(item.getImage_url());
            beerItemModel.setYeast(item.getYeast());
            beerItemModel.setValue(item.getValue());
            beerItemModel.setUnit(item.getUnit());
            beerItemModel.setAbv(item.getAbv());
            beerItemModel.setIbu(item.getIbu());
            beerItemModel.setEbc(item.getEbc());
            beerItemModel.setPh(item.getPh());
            beerItemModel.setTarget_og(item.getTarget_og());
            beerItemModel.setTarget_fg(item.getTarget_fg());
            beerItemModel.setFirst_brewed(item.getFirst_brewed());
            beerItemModel.setBrewers_tips(item.getBrewers_tips());
            beerItemModel.setContributed_by(item.getContributed_by());
            beerItemModel.setFoodPairing(item.getFood_pairing());

            localList.add(beerItemModel);
        });
        executeBeerData.onChanged(new Pair<>(new BaseModel(200, ""), localList));
    }
}

