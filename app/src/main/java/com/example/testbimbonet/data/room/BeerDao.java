package com.example.testbimbonet.data.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BeerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void inserAll(List<BeerEntity> beerListRoom);

    @Query("SELECT * FROM beers_table")
    List<BeerEntity> getBeerListRoom();
}
