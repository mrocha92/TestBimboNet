package com.example.testbimbonet.data.network.repository;

import android.util.Pair;

import androidx.lifecycle.Observer;

import com.example.testbimbonet.data.generic.BaseModel;
import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;

import java.util.List;

public interface BeersRepository {
      void getBeersList(String page, Observer<Pair<BaseModel, List<GetBeersResponseModelItem>>> executeBeerData);
}
