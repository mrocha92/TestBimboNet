package com.example.testbimbonet.data.service;


import com.example.testbimbonet.domain.network.response.bears.GetBeersResponseModelItem;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GenericServices {
    @GET("v2/beers?")
    Call<List<GetBeersResponseModelItem>> getBearsList(
            @Query("page") String page
    );

}
