package com.example.testbimbonet.data.generic;

public class BaseModel {
    public int status;
    public String message;


    public BaseModel() {
    }

    public BaseModel(int status, String message) {
        this.status = status;
        this.message = message;
    }

}