package com.example.testbimbonet.domain.local;


import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {
    private final Context context;


    public PreferencesHelper(Context context) {
        this.context = context;
    }


    public void setSharedPreferenceString(String fileName, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setSharedPreferenceInt(String fileName, String key, int value) {
        SharedPreferences settings = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public String getSharedPreferenceString(String fileName, String key, String defValue) {
        SharedPreferences settings = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        return settings.getString(key, defValue);
    }

    public int getSharedPreferenceInt(String fileName, String key, int defValue) {
        SharedPreferences settings = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        return settings.getInt(key, defValue);
    }

    public void cleanSharedPreference (String prefFile, String key) {
        SharedPreferences.Editor editor = context.getSharedPreferences(prefFile, Context.MODE_PRIVATE).edit();
        editor.remove(key).apply();
    }
}
