package com.example.testbimbonet.domain.network.response.bears;


public class  Volume {
   String unit;
   int value;

   public String getUnit() {
      return unit;
   }

   public int getValue() {
      return value;
   }

   public void setUnit(String unit) {
      this.unit = unit;
   }

   public void setValue(int value) {
      this.value = value;
   }
}
