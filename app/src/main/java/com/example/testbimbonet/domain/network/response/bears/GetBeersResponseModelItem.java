package com.example.testbimbonet.domain.network.response.bears;

import com.example.testbimbonet.domain.network.response.BaseResponse;

import java.util.ArrayList;
import java.util.Objects;

public class GetBeersResponseModelItem extends BaseResponse {
    public double abv;
    public double attenuation_level;
    public BoilVolume boil_volume;
    public String brewers_tips;
    public String contributed_by;
    public String description;
    public int ebc;
    public String first_brewed;
    public ArrayList<String> food_pairing;
    public double ibu;
    public int id;
    public String image_url;
    public Ingredients ingredients;
    public String name;
    public double ph;
    public double srm;
    public String tagline;
    public int target_fg;
    public double target_og;
    public Volume volume;
    private String yeast;
    private String foodPairing;

    private int value;

    private String unit;

    public GetBeersResponseModelItem() {
    }


    public String getFoodPairing() {
        return foodPairing;
    }

    public void setFoodPairing(String foodPairing) {
        this.foodPairing = foodPairing;
    }

    public String getYeast() {
        return yeast;
    }

    public int getValue() {
        return value;
    }
    public String getUnit() {
        return unit;
    }

    public void setValue(int value) {
        this.value  = value;
    }
    public void setUnit(String unit) {
        this.unit  = unit;
    }

    public void setYeast(String yeast) {
        this.yeast = yeast;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getFirst_brewed() {
        return first_brewed;
    }

    public void setFirst_brewed(String first_brewed) {
        this.first_brewed = first_brewed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public double getIbu() {
        return ibu;
    }

    public void setIbu(double ibu) {
        this.ibu = ibu;
    }

    public int getTarget_fg() {
        return target_fg;
    }

    public void setTarget_fg(int target_fg) {
        this.target_fg = target_fg;
    }

    public double getTarget_og() {
        return target_og;
    }

    public void setTarget_og(double target_og) {
        this.target_og = target_og;
    }

    public int getEbc() {
        return ebc;
    }

    public void setEbc(int ebc) {
        this.ebc = ebc;
    }

    public double getSrm() {
        return srm;
    }

    public void setSrm(double srm) {
        this.srm = srm;
    }

    public double getPh() {
        return ph;
    }

    public void setPh(double ph) {
        this.ph = ph;
    }

    public double getAttenuation_level() {
        return attenuation_level;
    }

    public void setAttenuation_level(double attenuation_level) {
        this.attenuation_level = attenuation_level;
    }

    public Volume getVolume() {
        return volume;
    }

    public void setVolume(Volume volume) {
        this.volume = volume;
    }

    public BoilVolume getBoil_volume() {
        return boil_volume;
    }

    public void setBoil_volume(BoilVolume boil_volume) {
        this.boil_volume = boil_volume;
    }


    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    public ArrayList<String> getFood_pairing() {
        return food_pairing;
    }

    public void setFood_pairing(ArrayList<String> food_pairing) {
        this.food_pairing = food_pairing;
    }

    public String getBrewers_tips() {
        return brewers_tips;
    }

    public void setBrewers_tips(String brewers_tips) {
        this.brewers_tips = brewers_tips;
    }

    public String getContributed_by() {
        return contributed_by;
    }

    public void setContributed_by(String contributed_by) {
        this.contributed_by = contributed_by;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetBeersResponseModelItem that = (GetBeersResponseModelItem) o;
        return id == that.id && Double.compare(that.abv, abv) == 0 && Double.compare(that.ibu, ibu) == 0 && target_fg == that.target_fg && Double.compare(that.target_og, target_og) == 0 && ebc == that.ebc && Double.compare(that.srm, srm) == 0 && Double.compare(that.ph, ph) == 0 && Double.compare(that.attenuation_level, attenuation_level) == 0 && name.equals(that.name) && tagline.equals(that.tagline) && first_brewed.equals(that.first_brewed) && description.equals(that.description) && image_url.equals(that.image_url) && volume.equals(that.volume) && boil_volume.equals(that.boil_volume)  && ingredients.equals(that.ingredients) && food_pairing.equals(that.food_pairing) && brewers_tips.equals(that.brewers_tips) && contributed_by.equals(that.contributed_by);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, tagline, first_brewed, description, image_url, abv, ibu, target_fg, target_og, ebc, srm, ph, attenuation_level, volume, boil_volume, ingredients, food_pairing, brewers_tips, contributed_by);
    }
}
