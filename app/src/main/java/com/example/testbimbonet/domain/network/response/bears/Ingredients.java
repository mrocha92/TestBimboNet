package com.example.testbimbonet.domain.network.response.bears;

import java.util.List;

public class Ingredients {
   List<Hop> hops;
   List<Malt> malt;
    String yeast;

    public List<Hop> getHops() {
        return hops;
    }
    public List<Malt> getMalt() {
        return malt;
    }
    public String getYeast() {
        return yeast;
    }
    public void setHops(List<Hop> hops) {
        this.hops = hops;
    }
    public void setMalt(List<Malt> malt) {
        this.malt = malt;
    }
    public void setYeast(String yeast) {
        this.yeast = yeast;
    }
}
