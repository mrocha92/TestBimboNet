package com.example.testbimbonet.domain.network.response;

public class BaseResponse {
    public int code;
    public String message;

    public BaseResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResponse() {
        this(0, "");
    }
}
